package academico.VO;

public class Usuario {
	private int cod_user;
	private String tipo_user;
	private String psw_user;
	
	public int getCod_user() {
		return cod_user;
	}
	
	public void setCod_user(int cod_user) {
		this.cod_user = cod_user;
	}
	
	public String getTipo_user() {
		return tipo_user;
	}
	
	public void setTipo_user(String tipo_user) {
		this.tipo_user = tipo_user;
	}
	
	public String getPsw_user() {
		return psw_user;
	}
	
	public void setPsw_user(String psw_user) {
		this.psw_user = psw_user;
	}
	
	
}
