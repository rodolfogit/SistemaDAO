package academico.VO;

public class Aluno {
	private int matricula_aluno;
	private String nome_aluno;
	private String turno;
	private Curso cod_curso;
	
	public int getMatricula_aluno() {
		return matricula_aluno;
	}
	public void setMatricula_aluno(int matricula_aluno) {
		this.matricula_aluno = matricula_aluno;
	}
	public String getNome_aluno() {
		return nome_aluno;
	}
	public void setNome_aluno(String nome_aluno) {
		this.nome_aluno = nome_aluno;
	}
	public String getTurno() {
		return turno;
	}
	public void setTurno(String turno) {
		this.turno = turno;
	}
	public int getCod_curso() {
		return cod_curso.getCod_curso();
	}
	public void setCod_curso(int cod_curso) {
		this.cod_curso.setCod_curso(cod_curso);
	}
	
	
}
