package academico;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

import academico.VO.Disciplina;
import conexao.Conexao;

public class TestConsole {

	public static void main(String[] args) {
				
		//public void buscaByNome() {
			 Connection con=Conexao.conectaBanco();
			 
		 try {
			 @SuppressWarnings("resource")
				Scanner ler = new Scanner(System.in);
				 
			 System.out.println("Digite um ID: ");
			 int IdAluno = ler.nextInt();
			 String query=
					 "select a.matricula_aluno, a.nome_aluno, d.nome_disciplina, valor_nota from sis_academico.nota n " + 
					 "inner join sis_academico.aluno a on a.matricula_aluno = n.matricula_aluno " + 
					 "inner join sis_academico.disciplina d on d.cod_disciplina = n.cod_disciplina " + 
					 "where a.matricula_aluno = ?";
			 PreparedStatement p = con.prepareStatement(query);
			 p.setInt(1, IdAluno);
			 ResultSet rs= p.executeQuery();

			 while (rs.next()){
					
					int matricula_aluno	 = rs.getInt(1);
					String nome_aluno = rs.getString(2);
					String nome_disciplina = rs.getString(3);
					int nota = rs.getInt(4);
					System.out.println("===================================================================");
					System.out.println("| IdAluno |     nome_aluno    |         Disciplina        |  nota |");
					System.out.println("-------------------------------------------------------------------");
					System.out.print("|     " +matricula_aluno);
					System.out.print("   |    " + nome_aluno);
					System.out.print("    |    " + nome_disciplina);
					System.out.println("    |  " + nota + "  |");
					System.out.println("-------------------------------------------------------------------");
					System.out.println("");
					}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		 
		
		
	}
}

