package academico.DAO;

import java.sql.ResultSet;
import java.util.List;

import academico.VO.Aluno;

public interface AlunoDAO {
		
	List<Aluno> listaAlunos();
	Aluno findById(int matricula_aluno);
	Aluno findByNome(String nome_aluno);
	public void viewBoletim(String sql);
	void cadastra(int matricula_aluno, String nome_aluno, String turno, int cod_curso);
	
	
	
	
}
